<?php

class Traduccion{
    public $es = array();
    public $ca = array();
    public $en = array(
        'Inicio'=>'Home',
        'Quienes somos'=>'About us',
        'Servicios'=>'Services',
        'Soporte'=>'Support',
        'Marcas'=>'Brands',
        'Galería de Productos'=>'Products Gallery',
        'Noticias / Eventos'=>'News / Events',
        'Contacténos'=>'Contacts',
        'Contactenos'=>'Contacts',
        'Ingles'=>'English',
        'Datos de contacto'=>'Conctact information',
        'Mensaje'=>'Message',
        'Nombre y Apellido'=>'Name and Surname',
        'Teléfono'=>'Phone number',
        'Enviar datos'=>'Submit information',
        'Leer más'=>'Read More'
        
    );

    public function traducir($view,$idioma = ''){
        if(!empty($idioma)){
            foreach($this->$idioma as $n=>$v){
                $view = str_replace($n,$v,$view);
            }
            return $view;
        }else{
            return $view;
        }
    }
}    
