<?php $this->load->view('includes/headerMain'); ?>
<?php $this->load->view('includes/banner'); ?>

<?php $productos = $this->db->get_where('productos',array('idioma'=>$_SESSION['lang'],'publicar_home'=>1)); ?>
<?php if($productos->num_rows>0): ?>
    <div class="carousel-wrap l-margin-tl">
        <div class="l-constrained carousel-wrap-inner">
            <ul class="carousel owl-carousel">
                <?php foreach($productos->result() as $p): ?>
                    <li class="carousel__item">
                        <a href="<?= site_url('e/'.toUrl($p->id.'-'.$p->nombre)) ?>" class="carousel__link">
                            <div class="carousel__image">
                                <img src="<?= base_url('img/productos/'.$p->foto) ?>" width="480" height="300" alt="<?= $p->nombre ?>">
                            </div>
                            <header class="carousel__header">
                                <h2 class="carousel__heading"><?= $p->nombre ?></h2>                                
                                <div class="carousel__description"><?= substr(strip_tags($p->contenido),0,100) ?></div>
                            </header>
                            <footer class="carousel__footer">
                                <span class="btn carousel__btn">Leer más</span>
                            </footer>
                        </a>
                    </li>
                <?php endforeach ?>
            </ul>
        </div>
    </div>
<?php endif ?>
<div style="margin-top:60px">
    <?php 
        
        $main = $this->db->get_where('paginas',array('titulo'=>'inicio','idioma'=>$_SESSION['lang']))->row()->contenido; 
        //Clientes
        $str = '<ul class="customer-logos">';
        foreach($this->db->get('clientes')->result() as $c){
            $str.= '<li class="customer-logos__item"><img width="250" height="150" alt="'.$c->nombre.'" src="'.base_url('img/clientes/'.$c->logo).'"></li>';
        }
        $str.= '</ul>';        
        $main = str_replace('[clientes]',$str,$main);
        
        //Multimedia
        $str = '<div class="l-container">';
            $this->db->limit(3);
            foreach($this->db->get_where('multimedia',array('idioma'=>$_SESSION['lang']))->result() as $m):
                $str.= '
                <div class="l-three-up no-bg featured-videos__video aligcenter">
                    '.$m->script.'
                    <h3 class="text-center"><a>'.$m->titulo.'</a></h3>
                </div>';
            endforeach;          
        $str.= '</div>';
        $main = str_replace('[multimedia]',$str,$main);
        
        //Noticias
        $str = '';
        $this->db->limit(3);
        foreach($this->db->get_where('noticias',array('idioma'=>$_SESSION['lang'],'tipo'=>1))->result() as $m): 
            $str .= '
            <div class="pod">
                <div class="pod__inner">
                    <a href="'.site_url('n/'.toUrl($m->id.'-'.$m->titulo)).'"> 
                        <img width="250" height="150" alt="'.$m->titulo.'" src="'.base_url('img/noticias/'.$m->foto).'" class="pod__image"> </a> 
                        <span class="pod__date">
                            '.$m->fecha.'
                        </span>
                        <p class="pod__heading pod__heading--lean">
                            <a target="_blank" href="'.site_url('n/'.toUrl($m->id.'-'.$m->titulo)).'" class="pod__link--oslo">
                                '.$m->titulo.'
                            </a>
                            <br data-mce-bogus="1">
                        </p>
                </div>
                <a href="'.site_url('n/'.toUrl($m->id.'-'.$m->titulo)).'" class="pod__cta-link">
                  Leer más
                </a>                
            </div>';
        endforeach; 
        $main = str_replace('[noticias]',$str,$main);
        
        //Noticias
        $str = '';
        $this->db->limit(3);
        foreach($this->db->get_where('noticias',array('idioma'=>$_SESSION['lang'],'tipo'=>2))->result() as $m): 
            $str .= '
            <div class="pod">
                <div class="pod__inner l-container">
                    <div class="l-one-third">
                        <div class="date-block"><span class="date-block__month">'.date("M",strtotime($m->fecha)).'</span> <span class="date-block__day">'.date("d",strtotime($m->fecha)).'</span></div>
                    </div>
                    <div class="l-two-thirds l-padding-ls">
                        <p class="pod__heading pod__heading--lean text-pebble">'.$m->titulo.'</p>                        
                        <a href="'.site_url('n/'.toUrl($m->id.'-'.$m->titulo)).'">Ver Mas </a></div>
                </div>
            </div>';
        endforeach; 
        $main = str_replace('[eventos]',$str,$main);
        
        
        //blog
        $str = '';
        $this->db->limit(3);
        foreach($this->db->get_where('blog',array('idioma'=>$_SESSION['lang']))->result() as $m): 
            $str .= '
            <div class="pod">
                <div class="pod__inner"><span class="pod__date">'.$m->fecha.'</span>
                    <p class="pod__heading pod__heading--lean">
                        <a href="'.site_url('b/'.toUrl($m->id.'-'.$m->titulo)).'" class="pod__link--oslo">'.$m->titulo.'</a>
                    </p>
                </div>
                <a href="'.site_url('b/'.toUrl($m->id.'-'.$m->titulo)).'">Ver mas</a>
            </div>';
        endforeach; 
        $main = str_replace('[blog]',$str,$main);
        
        //prensa
        $str = '';
        $this->db->limit(3);
        foreach($this->db->get_where('prensa',array('idioma'=>$_SESSION['lang']))->result() as $m): 
            $str .= '
            <div class="pod">
                <div class="pod__inner"><span class="pod__date">'.$m->fecha.'</span>
                    <p class="pod__heading pod__heading--lean">
                        <a href="'.site_url('pr/'.toUrl($m->id.'-'.$m->titulo)).'" class="pod__link--oslo">'.$m->titulo.'</a>
                    </p>
                </div>
                <a href="'.site_url('pr/'.toUrl($m->id.'-'.$m->titulo)).'">Ver mas</a>
            </div>';
        endforeach; 
        $main = str_replace('[prensa]',$str,$main);
        echo $main;
    ?>
</div>
                
