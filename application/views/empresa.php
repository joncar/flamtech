<div id="page-hero-container">
    <div class="page-hero" style="background-image:url(<?= base_url('img/paginas').'/'.$p->fondo ?>)"></div>
    <div class="slideshowShadow"></div>
    <div class="home-header menuontop" id='menu'>
        <div class="col-xs-12" id="homelogo2" style="text-align:center;">
             <a href="<?= site_url('food') ?>">
                <?= img('img/logo.png','width:20%') ?>
            </a>
        </div>
        <div class="col-lg-3 hidden-xs home-logo osLight" id="homelogo">
            <a href="<?= site_url('food') ?>">
                <?= img('img/logo.png') ?>
            </a>
        </div>
        
        <?php $this->load->view('includes/fragmentos/menu'); ?>
    </div>
    <div class="page-caption">
        <div class="page-title">Empresa</div>
    </div>
</div>

<div id="page" class="page-wrapper" id="">
    <div class="page-content">
        <div class="row" style="margin-left:0px; margin-right:0px">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="post-5447 page type-page status-publish has-post-thumbnail hentry" id="post-5447">
                    <div class="entry-content">
                        <?= $p->texto ?>
                        <div class="clearfix"></div>
                    </div>
                </div>                
            </div>
        </div>
    </div>
</div>
<?php if(isset($this->gamas)): ?>
<?= $this->load->view('includes/footer') ?>
<?php endif ?>
<script>
    $(document).ready(function () {
        $(window).on('scroll', function () {
            if ($(this).scrollTop() > $("#page").position().top - 50)
                $("#menu").addClass('menuonbottom').removeClass('menuontop');
            else
                $("#menu").addClass('menuontop').removeClass('menuonbottom');
        });

        $("a[href*=#]").click(function (e) {
            e.preventDefault();
            var target = $(this).attr('href');
            target = target.split("#");
            target = $("#" + target[1]);

            $('html, body').stop().animate({'scrollTop': parseInt(target.offset().top) - 30}, 900, 'swing');
        })
    });
</script>