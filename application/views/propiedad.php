<?php $this->load->view('includes/headerMain'); ?>
<div id="wrapper">
    <div id="mapView">
        <div class="mapPlaceholder"><span class="fa fa-spin fa-spinner"></span> Loading map...</div>
    </div>   
    <div id="content">
        <div class="filter">
            <h3>Filter your results</h3>
            <a href="javascript:void(0);" class="handleFilter"><span class="icon-equalizer"></span></a>
            <div class="clearfix"></div>
            <form class="filterForm" id="filterPropertyForm" role="search" method="get" action="">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <label>City</label>
                            <input type="text" class="form-control" name="search_city" id="search_city" value="San Francisco" placeholder="Enter city" autocomplete="off" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 formItem">
                        <div class="formField">
                            <label>Price Range</label>
                            <input type="hidden" name="search_min_price" id="search_min_price" value="" />
                            <input type="hidden" name="search_max_price" id="search_max_price" value="" />
                            <div class="slider priceSlider">
                                <div class="sliderTooltip">
                                    <div class="stArrow"></div>
                                    <div class="stLabel"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 formItem">
                        <div class="form-group fg-inline">
                            <label for="search_category">Category</label>
                            <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-o btn-light-gray dropdown-toggle">
                                <span class="dropdown-label">Category</span>&nbsp;&nbsp;&nbsp;<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-select">
                                <li class="active"><input type="radio" name="search_category" value="0" checked="checked" ><a href="javascript:void(0);">Category</a></li>
                                <li><input type="radio" name="search_category" value="29" ><a href="javascript:void(0);">Apartment</a></li>
                                <li><input type="radio" name="search_category" value="30" ><a href="javascript:void(0);">House</a></li>
                                <li><input type="radio" name="search_category" value="31" ><a href="javascript:void(0);">Land</a></li>
                            </ul>
                        </div>
                        <div class="form-group fg-inline">
                            <label for="search_type">Type</label>
                            <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-o btn-light-gray dropdown-toggle">
                                <span class="dropdown-label">Type</span>&nbsp;&nbsp;&nbsp;<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-select">
                                <li class="active"><input type="radio" name="search_type" value="0" checked="checked" ><a href="javascript:void(0);">Type</a></li>
                                <li><input type="radio" name="search_type" value="10" ><a href="javascript:void(0);">For Rent</a></li>
                                <li><input type="radio" name="search_type" value="11" ><a href="javascript:void(0);">For Sale</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="advancedFilter">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 formItem">
                            <div class="form-group fg-inline">
                                <label>Bedrooms</label>
                                <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-o btn-light-gray dropdown-toggle">
                                    <span class="dropdown-label">Bedrooms</span>&nbsp;&nbsp;&nbsp;<span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-select">
                                    <li class="active"><input type="radio" name="search_bedrooms" value="0" checked="checked" ><a href="javascript:void(0);">Bedrooms</a></li>
                                    <li><input type="radio" name="search_bedrooms" value="1"  ><a href="javascript:void(0);">1+</a></li>
                                    <li><input type="radio" name="search_bedrooms" value="2"  ><a href="javascript:void(0);">2+</a></li>
                                    <li><input type="radio" name="search_bedrooms" value="3"  ><a href="javascript:void(0);">3+</a></li>
                                    <li><input type="radio" name="search_bedrooms" value="4"  ><a href="javascript:void(0);">4+</a></li>
                                    <li><input type="radio" name="search_bedrooms" value="5"  ><a href="javascript:void(0);">5+</a></li>
                                </ul>
                            </div>
                            <div class="form-group fg-inline">
                                <label>Bathrooms</label>
                                <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-o btn-light-gray dropdown-toggle">
                                    <span class="dropdown-label">Bathrooms</span>&nbsp;&nbsp;&nbsp;<span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-select">
                                    <li class="active"><input type="radio" name="search_bathrooms" value="0" checked="checked"><a href="javascript:void(0);">Bathrooms</a></li>
                                    <li><input type="radio" name="search_bathrooms" value="1"  ><a href="javascript:void(0);">1+</a></li>
                                    <li><input type="radio" name="search_bathrooms" value="2"  ><a href="javascript:void(0);">2+</a></li>
                                    <li><input type="radio" name="search_bathrooms" value="3"  ><a href="javascript:void(0);">3+</a></li>
                                    <li><input type="radio" name="search_bathrooms" value="4"  ><a href="javascript:void(0);">4+</a></li>
                                    <li><input type="radio" name="search_bathrooms" value="5"  ><a href="javascript:void(0);">5+</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 formItem">
                            <div class="formField">
                                <label>Area Range</label>
                                <input type="hidden" name="search_min_area" id="search_min_area" value="" />
                                <input type="hidden" name="search_max_area" id="search_max_area" value="" />
                                <div class="slider areaSlider">
                                    <div class="sliderTooltip">
                                        <div class="stArrow"></div>
                                        <div class="stLabel"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 formItem">
                            <div class="form-group">
                                <label for="search_neighborhood">Neighborhood</label>
                                <input type="text" class="form-control" name="search_neighborhood" id="search_neighborhood" value="" placeholder="Enter neighborhood" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 formItem">
                            <div class="form-group">
                                <label>Amenities</label>
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                        <div class="checkbox custom-checkbox">
                                            <label><input type="checkbox" name="garage" value="1"  />
                                                <span class="fa fa-check"></span> Garage</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                        <div class="checkbox custom-checkbox">
                                            <label><input type="checkbox" name="security_system" value="1"  />
                                                <span class="fa fa-check"></span> Security System</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                        <div class="checkbox custom-checkbox">
                                            <label><input type="checkbox" name="air_conditioning" value="1"  />
                                                <span class="fa fa-check"></span> Air Conditioning</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                        <div class="checkbox custom-checkbox">
                                            <label><input type="checkbox" name="balcony" value="1"  />
                                                <span class="fa fa-check"></span> Balcony</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                        <div class="checkbox custom-checkbox">
                                            <label><input type="checkbox" name="outdoor_pool" value="1"  />
                                                <span class="fa fa-check"></span> Outdoor Pool</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                        <div class="checkbox custom-checkbox">
                                            <label><input type="checkbox" name="internet" value="1"  />
                                                <span class="fa fa-check"></span> Internet</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>        
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <a href="javascript:void(0);" class="btn btn-green mb-10" id="filterPropertySubmit">Apply Filter</a>
                            <a href="javascript:void(0);" class="btn btn-gray display mb-10" id="showAdvancedFilter">Show Advanced Filter Options</a>
                            <a href="javascript:void(0);" class="btn btn-gray mb-10" id="hideAdvancedFilter">Hide Advanced Filter Options</a>
                        </div>
                    </div>
                </div>
            </form>
            <div class="clearfix"></div>
        </div>
        <div class="resultsList">
            <h1 class="pull-left">Properties Search Results</h1>
            <div class="pull-right sort">
                <div class="form-group">
                    Sort by:&nbsp;&nbsp;
                    <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-white dropdown-toggle">
                        <span class="dropdown-label">Newest</span>&nbsp;&nbsp;&nbsp;<span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-select sorter">
                        <li class="active"><input type="radio" name="sort" value="newest" checked="checked" ><a href="javascript:void(0);">Newest</a></li>
                        <li><input type="radio" name="sort" value="price_lo"  ><a href="javascript:void(0);">Price (Lo-Hi)</a></li>
                        <li><input type="radio" name="sort" value="price_hi"  ><a href="javascript:void(0);">Price (Hi-Lo)</a></li>
                        <li><input type="radio" name="sort" value="bedrooms"  ><a href="javascript:void(0);">Bedrooms</a></li>
                        <li><input type="radio" name="sort" value="bathrooms"  ><a href="javascript:void(0);">Bathrooms</a></li>
                        <li><input type="radio" name="sort" value="area"  ><a href="javascript:void(0);">Area</a></li>
                    </ul>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <a href="http://mariusn.com/themes/reales-wp/properties/sophisticated-residence/" class="card" id="card-309">
                        <div class="figure">
                            <div class="featured-label">
                                <div class="featured-label-left"></div>
                                <div class="featured-label-content"><span class="fa fa-star"></span></div>
                                <div class="featured-label-right"></div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="img" style="background-image:url(http://mariusn.com/themes/reales-wp/wp-content/uploads/2015/02/img-prop.jpg);"></div>
                            <div class="figCaption">
                                <div>$799,000</div>
                                <span><span class="icon-eye"></span> 9999+</span>
                                <span><span class="icon-heart"></span> 2</span>
                                <span><span class="icon-bubble"></span> 1</span>
                            </div>
                            <div class="figView"><span class="icon-eye"></span></div>
                            <div class="figType">For Sale</div>
                        </div>
                        <h2>Sophisticated Residence</h2>
                        <div class="cardAddress">
                            600 40th Ave, Richmond District, San Francisco<br />CA, 94121, United States                        </div>
                        <ul class="cardFeat">
                            <li><span class="fa fa-moon-o"></span> 2</li>
                            <li><span class="icon-drop"></span> 3</li>
                            <li><span class="icon-frame"></span> 1270 sq ft</li>
                        </ul>
                        <div class="clearfix"></div>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <a href="http://mariusn.com/themes/reales-wp/properties/luxury-mansion/" class="card" id="card-312">
                        <div class="figure">
                            <div class="featured-label">
                                <div class="featured-label-left"></div>
                                <div class="featured-label-content"><span class="fa fa-star"></span></div>
                                <div class="featured-label-right"></div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="img" style="background-image:url(http://mariusn.com/themes/reales-wp/wp-content/uploads/2014/12/bg-5-1024x576.jpg);"></div>
                            <div class="figCaption">
                                <div>$3,400/mo</div>
                                <span><span class="icon-eye"></span> 9999+</span>
                                <span><span class="icon-heart"></span> 2</span>
                                <span><span class="icon-bubble"></span> 1</span>
                            </div>
                            <div class="figView"><span class="icon-eye"></span></div>
                            <div class="figType">For Rent</div>
                        </div>
                        <h2>Luxury Mansion</h2>
                        <div class="cardAddress">
                            10 Romain St, Twin Peaks, San Francisco<br />CA, 123456, Romania                        </div>
                        <ul class="cardFeat">
                            <li><span class="fa fa-moon-o"></span> 2</li>
                            <li><span class="icon-drop"></span> 1</li>
                            <li><span class="icon-frame"></span> 1220 sq ft</li>
                        </ul>
                        <div class="clearfix"></div>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <a href="http://mariusn.com/themes/reales-wp/properties/elegant-apartment/" class="card" id="card-174">
                        <div class="figure">
                            <div class="featured-label">
                                <div class="featured-label-left"></div>
                                <div class="featured-label-content"><span class="fa fa-star"></span></div>
                                <div class="featured-label-right"></div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="img" style="background-image:url(http://mariusn.com/themes/reales-wp/wp-content/uploads/2014/12/bg-4.jpg);"></div>
                            <div class="figCaption">
                                <div>$895,000</div>
                                <span><span class="icon-eye"></span> 9999+</span>
                                <span><span class="icon-heart"></span> 0</span>
                                <span><span class="icon-bubble"></span> 1</span>
                            </div>
                            <div class="figView"><span class="icon-eye"></span></div>
                            <div class="figType">For Sale</div>
                        </div>
                        <h2>Elegant Apartment</h2>
                        <div class="cardAddress">
                            1485 Guerrero St, Noe Valley, San Francisco<br />CA, 94110, United States                        </div>
                        <ul class="cardFeat">
                            <li><span class="fa fa-moon-o"></span> 2</li>
                            <li><span class="icon-drop"></span> 1</li>
                            <li><span class="icon-frame"></span> 1200 sq ft</li>
                        </ul>
                        <div class="clearfix"></div>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <a href="http://mariusn.com/themes/reales-wp/properties/modern-residence/" class="card" id="card-313">
                        <div class="figure">
                            <div class="img" style="background-image:url(http://mariusn.com/themes/reales-wp/wp-content/uploads/2014/12/bg-1.jpg);"></div>
                            <div class="figCaption">
                                <div>$23,234</div>
                                <span><span class="icon-eye"></span> 9999+</span>
                                <span><span class="icon-heart"></span> 3</span>
                                <span><span class="icon-bubble"></span> 1</span>
                            </div>
                            <div class="figView"><span class="icon-eye"></span></div>
                            <div class="figType">For Sale</div>
                        </div>
                        <h2>Modern Residence</h2>
                        <div class="cardAddress">
                            547 35th Ave, Richmond District, San Francisco<br />CA, 94121, United States                        </div>
                        <ul class="cardFeat">
                            <li><span class="fa fa-moon-o"></span> 4</li>
                            <li><span class="icon-drop"></span> 4</li>
                            <li><span class="icon-frame"></span> 2225 sq ft</li>
                        </ul>
                        <div class="clearfix"></div>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <a href="http://mariusn.com/themes/reales-wp/properties/futuristic-apartment/" class="card" id="card-172">
                        <div class="figure">
                            <div class="img" style="background-image:url(http://mariusn.com/themes/reales-wp/wp-content/uploads/2015/01/blog-4.jpg);"></div>
                            <div class="figCaption">
                                <div>$5,400/mo</div>
                                <span><span class="icon-eye"></span> 9999+</span>
                                <span><span class="icon-heart"></span> 2</span>
                                <span><span class="icon-bubble"></span> 1</span>
                            </div>
                            <div class="figView"><span class="icon-eye"></span></div>
                            <div class="figType">For Rent</div>
                        </div>
                        <h2>Futuristic Apartment</h2>
                        <div class="cardAddress">
                            333 Bush St #3804, Financial District, San Francisco<br />CA, 94104, United States                        </div>
                        <ul class="cardFeat">
                            <li><span class="fa fa-moon-o"></span> 2</li>
                            <li><span class="icon-drop"></span> 2</li>
                            <li><span class="icon-frame"></span> 1510 sq ft</li>
                        </ul>
                        <div class="clearfix"></div>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <a href="http://mariusn.com/themes/reales-wp/properties/stylish-apartment/" class="card" id="card-167">
                        <div class="figure">
                            <div class="img" style="background-image:url(http://mariusn.com/themes/reales-wp/wp-content/uploads/2014/12/bg-3.jpg);"></div>
                            <div class="figCaption">
                                <div>$4,800/mo</div>
                                <span><span class="icon-eye"></span> 7242 </span>
                                <span><span class="icon-heart"></span> 0</span>
                                <span><span class="icon-bubble"></span> 1</span>
                            </div>
                            <div class="figView"><span class="icon-eye"></span></div>
                            <div class="figType">For Rent</div>
                        </div>
                        <h2>Stylish Apartment</h2>
                        <div class="cardAddress">
                            2200 Pacific Ave #7D, Pacific Heights, San Francisco<br />CA, 94115, United States                        </div>
                        <ul class="cardFeat">
                            <li><span class="fa fa-moon-o"></span> 2</li>
                            <li><span class="icon-drop"></span> 2</li>
                            <li><span class="icon-frame"></span> 1850 sq ft</li>
                        </ul>
                        <div class="clearfix"></div>
                    </a>
                </div>
            </div>
            <div class="pull-left">
            </div>
            <div class="pull-right search_prop_calc">
                1 - 6 of 6 Properties found            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>