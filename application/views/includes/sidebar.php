<?php if($this->user->log): ?>
<div id="sidebar" class="sidebar responsive">
        <ul class="nav nav-list">
            <li class="active highlight">
                <a href="<?= site_url('panel') ?>">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text">Escritorio</span>
                </a>
                <b class="arrow"></b>
            </li>
             <!--- Alumnos --->
             <?php 
                    $menu = array(
                        'noticias'=>array('admin/noticias'),
                        'clientes'=>array('admin/clientes'),
                        'productos'=>array('admin/productos'),
                        'multimedia'=>array('admin/multimedia'),
                        'blog'=>array('admin/blog'),
                        'prensa'=>array('admin/prensa'),
                        'paginas'=>array('admin/paginas','admin/banner'),
                        'seguridad'=>array('grupos','funciones','user'),
                        'admin'=>array('ajustes'),
                        'multimedia'=>array('admin/multimedia'),
                        'soporte'=>array('admin/areas_servicio','admin/soporte')
                    );
                    $menu = $this->user->filtrarMenu($menu);
                    $label = array(
                        'noticias'=>array('Noticias','fa fa-newspaper-o'),
                        'productos'=>array('Productos','fa fa-cubes'),
                        'paginas'=>array('CMS','fa fa-object-group'),
                        'seguridad'=>array('Seguridad','fa fa-user-secret'),
                        'admin'=>array('Ajustes','fa fa-wrench'),
                        'multimedia'=>array('Multimedia','fa fa-play'),
                        'clientes'=>array('Clientes','fa fa-user-md'),
                        'soporte'=>array('Soporte','fa fa-ticket'),
                        'blog'=>array('Blog','fa fa-rss'),
                        'prensa'=>array('Prensa','fa fa-file-text')
                    );
             ?>
             <?php  echo getMenu($menu,$label); ?>                 
        </ul>
       <div id="sidebar-collapse" class="sidebar-toggle sidebar-collapse">
            <i data-icon2="ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" class="ace-icon fa fa-angle-double-left"></i>
        </div>

        <script type="text/javascript">
                try{ace.settings.check('sidebar' , 'collapsed')
                ace.settings.sidebar_collapsed(true, true);
                }catch(e){}
        </script>
</div>
<?php endif ?>
