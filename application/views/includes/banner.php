<div class="hero">
    <div class="hero__content hero__content--centered">
        <div class="hero__main">
            <div class="slideshow">
                <ul class="slides" style="min-height:360px;">
                    <?php foreach($this->db->get('banner')->result() as $b): ?>
                        <li class="slide" style="background-image: url(<?= base_url('img/banner/'.$b->foto) ?>);">
                            <div class="l-constrained--site-constraint">
                                <div class="l-primary layout-one slide__text">
                                    <h2 class="slide__heading"><?= $b->titulo ?></h2>
                                    <p class="slide__subheading"><?= $b->texto ?></p>                                    
                                </div>                                
                            </div>
                        </li>
                    <?php endforeach ?>
                </ul>
                <div class="slide__pager"></div>
            </div>
        </div>
        <div class="hero__aside">
            <div class="engagement l-constrained">
                <p class="engagement__intro"><em>Somos Flamtech</em></p>
                <div class="drop-select">
                    <span class="drop-select__label">Seguridad</span>
                    <ul class="drop-select__list">
                        <li class="drop-select__item"><a class="" href="<?= site_url('p/servicios') ?>" data-selection="<?= site_url('p/servicios') ?>"> Consultoría</a></li>
                        <li class="drop-select__item"><a class="" href="<?= site_url('p/servicios') ?>" data-selection="<?= site_url('p/servicios') ?>">Ingeniería</a></li>
                        <li class="drop-select__item"><a class="" href="<?= site_url('p/servicios') ?>" data-selection="<?= site_url('p/servicios') ?>">Proveedor</a></li>                        
                    </ul>
                </div>                
            </div>
        </div>
    </div>
</div>
