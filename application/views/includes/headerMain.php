<header class="l-constrained head-top" style="max-width: none; padding: 0px 40px;">
    <div class="head-top-meta">
        <div id="top">
            <div class="menu-btn menu-btn--open">
                <a id="nav-open-btn" class="menu-btn__icon fa fa-bars" href="#nav"><span class="visuallyhidden">Open Menu</span></a>
            </div>
        </div>

        <ul class="menu menu-head">
            <li><a href="<?= base_url('main/traducir/en') ?>">Ingles</a> <span class="pipe">|</span> </li>
            <li><a href="<?= base_url('main/traducir/es') ?>">Español</a> <span class="pipe">|</span> </li>            
        </ul>
    </div>
    <div class="logo-wrap">
        <a href="#"><img class="logo-main__image" src="<?= base_url('img/logo.jpg') ?>" alt="Lantronix" style="max-width:371px"></a>        
    </div>
</header>
<nav>
    <div class="l-constrained" style="max-width: none; padding: 0px 40px;">
        <div class="menu-block">
            <nav id="nav" class="nav-primary">
                <div class="menu-btn menu-btn--close">
                    <a id="nav-close-btn" class="menu-btn__icon fa fa-times" href="#top"><span class="visuallyhidden">Close Menu</span></a>
                </div>

                <ul class="menu menu-main">
                    <li><a href="<?= site_url() ?>">Inicio</a></li>
                    <li><a href="<?= site_url('p/quienes-somos') ?>">Quienes somos</a></li>
                    <li><a href="<?= site_url('p/servicios') ?>">Servicios</a></li>
                    <li><a href="<?= site_url('s/soporte') ?>">Soporte</a></li>                    
                    <li><a href="<?= site_url('p/marcas') ?>">Marcas</a></li>
                    <li><a href="<?= site_url('e/productos') ?>">Galería de Productos</a></li>
                    <li><a href="<?= site_url('n/noticias') ?>">Noticias / Eventos</a></li>
                    <li><a href="<?= site_url('c/contactenos') ?>">Contacténos</a></li>
                </ul>
            </nav>
        </div>
        <div class="search-block" style="text-align:center">
            RIF <?= $this->db->get_where('ajustes')->row()->rif ?>
        </div>
    </div>
</nav>