<div class="modal fade" id="signin" role="dialog" aria-labelledby="signinLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="signinLabel">Entrar</h4>
            </div>
            <div class="modal-body">
                <div class="form-container">
                    <form role="form" action="<?= base_url('main/login') ?>" onsubmit="return validar(this)" method="post">   
                        <div class="row" style="margin:5px;">
                            <input type="email" placeholder="Email" data-val="required" id="field-email" name="email" class="form-control">
                       </div>
                       <div class="row" style="margin:5px;">
                            <input type="password" placeholder="Contraseña" data-val="required" id="field-pass" name="pass" class="form-control">
                       </div>
                       <input type="hidden" name="redirect" value="<?= base_url(str_replace('webnew/','',$_SERVER['REQUEST_URI'])) ?>">
                       <div align="center">
                           <button type="submit" class="btn btn-green mb-10">Entrar</button>                            
                            <a class="btn btn-link" href="#" data-toggle="modal" data-target="#forgot">¿Olvidastes tu contraseña?</a><br/>
                            <a href="#" data-toggle="modal" data-target="#signup">Registrarse</a>
                       </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="signup" role="dialog" aria-labelledby="signupLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="signupLabel">Registrate</h4>
            </div>
            <div class="modal-body">
                <form accept-charset="utf-8" enctype="multipart/form-data" autocomplete="off" id="crudForm" method="post" onsubmit="return registrar()">
                    <div class="row" style="margin:5px;">
                        <input type="text" placeholder="Nombre" data-val="required" id="field-nombre" name="nombre" class="form-control">
                   </div>
                    <div class="row" style="margin:5px;">
                        <input type="text" placeholder="Apellidos" data-val="required" id="field-apellidos" name="apellidos" class="form-control">
                   </div>
                    <div class="row" style="margin:5px;">
                        <input type="email" placeholder="Email de contacto" data-val="required" id="field-email" name="email" class="form-control">
                   </div>
                   <div class="row" style="margin:5px;">
                        <input type="password" placeholder="Contraseña nuevo usuario" data-val="required" id="field-password" name="password" class="form-control">
                   </div>
                    <!-- Start of hidden inputs -->
                    <!-- End of hidden inputs -->

                    <div class="alert alert-danger" style="display:none" id="report-error"></div>
                    <div class="alert alert-success" style="display:none" id="report-success"></div>

                    <div align="center">			
                        <button type="submit" class="btn btn-green mb-10">Registrarse</button>
                    </div>                
                </form>
            </div>
        </div>
    </div>
    <script>
        function registrar(){
            var form = document.getElementById('crudForm');
            var data = new FormData(form);
            $.ajax({
                url: '<?= base_url('registro/index/insert_validation') ?>',
                data: data,
                type:'POST',
                processData:false,
                cache:false,
                contentType:false,
                success:function(data){
                    data = data.replace('<textarea>','');
                    data = data.replace('</textarea>','');
                    data = JSON.parse(data);
                    if(data.success){
                        var form = document.getElementById('crudForm');
                        var data = new FormData(form);
                        $.ajax({
                            url: '<?= base_url('registro/index/insert') ?>',
                            data: data,
                            type:'POST',
                            processData:false,
                            cache:false,
                            contentType:false,
                            success:function(data){
                                document.location.href="<?= base_url(str_replace('webnew/','',$_SERVER['REQUEST_URI'])) ?>";
                            }
                      });
                    }else{
                        alert('Ha ocurrido un error al añadir el usuario, por favor verifique los campos e intentelo de nuevo');
                    }
                }
            });
            return false;
        }
    </script>
</div>

<div class="modal fade" id="forgot" role="dialog" aria-labelledby="forgotLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="forgotLabel">¿Olvidastes tu contraseña?</h4>
            </div>
            <div class="modal-body">
                <div class="alerta" style="display:none;"></div>
                <form id="crudFormForgot" method="post" onsubmit="return forgot()" role="form" class="form-horizontal">
                    <?= !empty($_SESSION['msj'])?$_SESSION['msj']:'' ?>
                    <?= !empty($msj)?$msj:'' ?>
                    <div class="row" style="margin:5px;">
                        <input type="email" placeholder="Email de contacto" data-val="required" id="field-email" name="email" class="form-control">
                   </div>      
                    <div align="center">			
                        <button type="submit" class="btn btn-green mb-10">Registrarse</button>
                    </div> 
                </form>
            </div>
        </div>
    </div>
    <script>
        function forgot(){
            var form = document.getElementById('crudFormForgot');
            var data = new FormData(form);
            $.ajax({
                url: '<?= base_url('registro/forget') ?>/0/ajax',
                data: data,
                type:'POST',
                processData:false,
                cache:false,
                contentType:false,
                success:function(data){
                    $("#forgot .alerta").html(data);
                    $("#forgot .alerta").show();
                }
            });
            return false;
        }
    </script>
</div>

<div class="modal fade" id="resetpass" role="dialog" aria-labelledby="resetpassLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="resetpassLabel">Resetear Password</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="userResetPassForm" method="post">
                    <div class="resetPassMessage" id="resetPassMessage"></div>
                    <div class="form-group">
                        <input type="password" name="resetPass_1" id="resetPass_1" placeholder="New Password" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" name="resetPass_2" id="resetPass_2" placeholder="Confirm Password" class="form-control">
                    </div>
                    <p class="help-block">Hint: The password should be at least seven characters long. To make it stronger, use upper and lower case letters, numbers, and symbols like ! " ? $ % ^ & ).</p>
                    <input type="hidden" id="securityResetpass" name="securityResetpass" value="027bc48fcb" /><input type="hidden" name="_wp_http_referer" value="/themes/reales-wp/" />                    <div class="form-group">
                        <div class="btn-group-justified">
                            <a href="#" class="btn btn-lg btn-green" id="submitResetPass">Reset Password</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="searches-modal" role="dialog" aria-labelledby="searches-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="fa fa-close"></span></button>
                <h4 class="modal-title" id="searches-label">Mis búsquedas</h4>
            </div>
            <div class="modal-body"></div>
            <input type="hidden" name="modal-user-id" id="modal-user-id" value="0">
            <input type="hidden" id="securityDeleteSearch" name="securityDeleteSearch" value="c7c8fbceae" /><input type="hidden" name="_wp_http_referer" value="/themes/reales-wp/" />            <div class="modal-footer">
                <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-gray">Cerrar</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="save-search-modal" role="dialog" aria-labelledby="save-search-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="fa fa-close"></span></button>
                <h4 class="modal-title" id="save-search-label">Save Search</h4>
            </div>
            <div class="modal-body">
                <form id="save-search-form">
                    <div class="save-search-message" id="save-search-message"></div>
                    <div class="form-group">
                        <label for="save-search-name">Name</label>
                        <input type="text" id="save-search-name" name="save-search-name" placeholder="Enter a name for your search" class="form-control">
                                                <input type="hidden" id="save-search-user" name="save-search-user" value="0">
                    </div>
                    <input type="hidden" id="securitySaveSearch" name="securitySaveSearch" value="104138ff8f" /><input type="hidden" name="_wp_http_referer" value="/themes/reales-wp/search-results/?search_city=San+Francisco&amp;search_lat=37.7749295&amp;search_lng=-122.41941550000001&amp;search_category=0&amp;search_type=0&amp;search_min_price=&amp;search_max_price=" />                </form>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-gray">Cancel</a>
                <a href="javascript:void(0);" class="btn btn-green" id="save-search-btn">Save</a>
            </div>
        </div>
    </div>
</div>