<?php if($this->router->fetch_class()!='registro'): ?>
<div class="topUserWraper">
    <a href="#" class="userNavHandler">
        <span class="icon-user"></span>
    </a>        
        <div class="user-nav">
            <ul>
                <?php if(empty($_SESSION['user'])): ?>
                <li><a data-target="#signup" data-toggle="modal" href="#" style="color:#999">Regístrate</a></li>
                <li><a data-target="#signin" data-toggle="modal" href="#" style="color:#999">Conéctate</a></li>
                <?php else: ?>
                <li><a href="<?= base_url('main/unlog') ?>">Salir</a></li>
                <?php endif ?>
            </ul>
        </div>    
</div>
<?php endif ?>
<?php $this->load->view('includes/fragmentos/modals'); ?>
<a href="javascript:void(0);" class="top-navHandler visible-xs">
    <span class="fa fa-bars"></span>
</a>
<div class="top-nav">
    <div class="menu-top-menu-container">
        <ul class="menu">
            <li>
                <a href="<?= site_url() ?>">Inicio</a>
            </li>
            <li><a href="<?= site_url('food/empresa') ?>">Empresa</a></li>
            <li><a href="<?= site_url('food/contacto') ?>">Contacto</a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Idioma <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="<?= site_url('main/traducir/es') ?>">Español</a></li>
                    <li><a href="<?= site_url('main/traducir/en') ?>">English</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>