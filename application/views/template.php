<!DOCTYPE html>
<html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= empty($title)?'FLAMTECH':$title ?></title>    
    <link rel='stylesheet' id='googlefonts-css'  href='//fonts.googleapis.com/css?family=Roboto:500,300,400,700' type='text/css' media='all' />
    <link rel='stylesheet' id='fontawesome-css'  href='//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css?ver=4.4.1' type='text/css' media='all' />
    <link rel='stylesheet' id='main-css'  href='<?= base_url('css/template')?>/main.css' type='text/css' media='all' />
    <link rel='stylesheet' id='ltrx015201-style-css'  href='<?= base_url('css/template')?>/style.css?ver=4.4.1' type='text/css' media='all' />
    <link rel='stylesheet' id='ltrx015201-style-css'  href='<?= base_url('css')?>/style.css?ver=4.4.1' type='text/css' media='all' />
    <script src="http://code.jquery.com/jquery-1.11.3.js"></script>
    <?php if(!empty($crud)): ?>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>                
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <?php endif ?>    
    <script type='text/javascript' src='<?= base_url('js/template')?>/jquery-migrate.min.js?ver=1.2.1'></script>
    <script type='text/javascript' src='<?= base_url('js/template')?>/modernizr-2.8.3.min.js'></script>
    </head>
    <body class="home">       
            <div class="shell">
                <?php $this->load->view($view); ?>
            </div><!-- / .shell -->
            <?php $this->load->view('includes/footer') ?>
    <script type='text/javascript' src='<?= base_url('js/template')?>/plugins.min.js'></script>    
    <script type='text/javascript' src='<?= base_url('js/template')?>/main.min.js'></script>    
</body>
</html>
