<?php
require_once APPPATH.'/controllers/main.php';
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
class Api extends Main {
        
        public function __construct()
        {
                parent::__construct();
                $this->load->library('grocery_crud');
                $this->load->library('ajax_grocery_crud');
                $this->load->model('querys');
        }
        
        public function loadView($data)
        {
             if(!empty($data->output)){
                $data->view = empty($data->view)?'panel':$data->view;
                $data->crud = empty($data->crud)?'user':$data->crud;
                $data->title = empty($data->title)?ucfirst($this->router->fetch_method()):$data->title;
            }
            parent::loadView($data);
        }
        
        protected function crud_function($x = '',$y = '',$controller = ''){
            $crud = new ajax_grocery_CRUD($controller);
            $crud->set_theme('bootstrap2');
            //$crud->set_model('api_model');
            $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
            $crud->set_table($table);
            $crud->set_subject(ucfirst($this->router->fetch_method()));            
            $crud->required_fields_array();
            return $crud;
        }
        
        //Foods
        public function destinatarios(){
            $crud = $this->crud_function();
            $crud->unset_delete()
                     ->unset_add()
                     ->unset_edit()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export();
            if(!empty($_POST['idioma'])){
                $crud->where('destinatarios.idioma',$_POST['idioma']);
            }
            $crud->columns('id','destinatarios_nombre','resumen','productos');
            $crud->callback_column('productos',function($val,$row){
               $productos = array();
               get_instance()->db->select('foods.*');
               get_instance()->db->join('foods_destinatarios','foods_id = foods.id');
               foreach(get_instance()->db->get_where('foods',array('destinatarios_id'=>$row->id))->result() as $p){                   
                   get_instance()->db->join('foods_destinatarios','destinatarios_id = destinatarios.id');
                   $res = get_instance()->db->get_where('destinatarios',array('foods_id'=>$p->id));
                   $d = array();
                   foreach($res->result() as $de){
                       $d[] = $de;
                   }
                   $p->destinatarios = $d;
                   
                   get_instance()->db->join('foods_aplicaciones','aplicaciones_id = aplicaciones.id');
                   $res = get_instance()->db->get_where('aplicaciones',array('foods_id'=>$p->id));
                   $d = array();
                   foreach($res->result() as $de){
                       $d[] = $de;
                   }
                   $p->aplicaciones = $d;
                   
                   
                   $productos[] = $p;
               }
               return json_encode($productos);
            });
            $crud->render();   
            $this->loadView($crud);
        }
        
        //Foods
        public function aplicaciones(){
            $crud = $this->crud_function();
            $crud->unset_delete()
                     ->unset_add()
                     ->unset_edit()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export();
            if(!empty($_POST['idioma'])){
                $crud->where('aplicaciones.idioma',$_POST['idioma']);
            }
            $crud->columns('id','aplicaciones_nombre','productos');
            $crud->callback_column('productos',function($val,$row){
               $productos = array();
               get_instance()->db->select('foods.*');
               get_instance()->db->join('foods_aplicaciones','foods_id = foods.id');
               foreach(get_instance()->db->get_where('foods',array('aplicaciones_id'=>$row->id))->result() as $p){                   
                   get_instance()->db->join('foods_destinatarios','destinatarios_id = destinatarios.id');
                   $res = get_instance()->db->get_where('destinatarios',array('foods_id'=>$p->id));
                   $d = array();
                   foreach($res->result() as $de){
                       $d[] = $de;
                   }
                   $p->destinatarios = $d;
                   
                   get_instance()->db->join('foods_aplicaciones','aplicaciones_id = aplicaciones.id');
                   $res = get_instance()->db->get_where('aplicaciones',array('foods_id'=>$p->id));
                   $d = array();
                   foreach($res->result() as $de){
                       $d[] = $de;
                   }
                   $p->aplicaciones = $d;
                   
                   
                   $productos[] = $p;
               }
               return json_encode($productos);
            });
            $crud->render();  
            $this->loadView($crud);
        }
        //Fin Foods
        //Gamas
        public function gamas(){
            $crud = $this->crud_function();
            $crud->unset_delete()
                     ->unset_add()
                     ->unset_edit()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export();
            if(!empty($_POST['idioma'])){
                $crud->where('gamas.idioma',$_POST['idioma']);
            }
            $crud->columns('id','gamas_nombre','productos');
            $crud->callback_column('productos',function($val,$row){
               $productos = array();
               foreach(get_instance()->db->get_where('foods',array('gamas_id'=>$row->id))->result() as $p){
                   $productos[] = $p;
               }
               return json_encode($productos);
            });
            $crud->render();  
            $this->loadView($crud);
        }
        
        public function gamasSearch(){
            $this->as = array('gamasSearch'=>'gamas');
            $crud = $this->crud_function();
            if(!empty($_POST['idioma'])){
                $crud->where('gamas.idioma',$_POST['idioma']);
            }
            $crud->unset_delete()
                     ->unset_add()
                     ->unset_edit()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export();            
            $crud->render();  
            $this->loadView($crud);
        }
        
        public function aplicacionesSearch(){
            $this->as = array('aplicacionesSearch'=>'aplicaciones');
            $crud = $this->crud_function();
            if(!empty($_POST['idioma'])){
                $crud->where('aplicaciones.idioma',$_POST['idioma']);
            }
            $crud->unset_delete()
                     ->unset_add()
                     ->unset_edit()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export();            
            $crud->render();  
            $this->loadView($crud);
        }
        
        public function destinatariosSearch(){
            $this->as = array('destinatariosSearch'=>'destinatarios');
            $crud = $this->crud_function();
            if(!empty($_POST['idioma'])){
                $crud->where('destinatarios.idioma',$_POST['idioma']);
            }
            $crud->unset_delete()
                     ->unset_add()
                     ->unset_edit()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export();            
            $crud->render();  
            $this->loadView($crud);
        }
        
        
        /*** Beverage ****/
        //Foods
        public function clasificacion(){
            $crud = $this->crud_function();
            if(!empty($_POST['idioma'])){
                $crud->where('clasificacion.idioma',$_POST['idioma']);
            }
            $crud->unset_delete()
                     ->unset_add()
                     ->unset_edit()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export();
            $crud->columns('id','clasificacion_nombre','resumen','productos');
            $crud->callback_column('productos',function($val,$row){
               $productos = array();
               get_instance()->db->select('beverages.*,clasificacion.clasificacion_nombre, clasificacion.imagen_footer as clasificacion_imagen_footer');
               get_instance()->db->join('clasificacion','clasificacion_id = clasificacion.id');
               foreach(get_instance()->db->get_where('beverages',array('clasificacion_id'=>$row->id))->result() as $p){                                      
                   $productos[] = $p;
               }
               return json_encode($productos);
            });
            $crud->render();   
            $this->loadView($crud);
        }
        
        //Foods
        public function funciones(){
            $this->as = array('funciones'=>'beverage_funciones');
            $crud = $this->crud_function();
            $crud->unset_delete()
                     ->unset_add()
                     ->unset_edit()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export();
            $crud->columns('id','beverage_funciones_nombre','resumen','productos');
            $crud->callback_column('productos',function($val,$row){
               $productos = array();               
               foreach(get_instance()->db->get_where('beverages',array($row->url=>1))->result() as $p){                                      
                   $productos[] = $p;
               }
               return json_encode($productos);
            });
            $crud->render();   
            $this->loadView($crud);
        }
        
        public function clasificacionesSearch(){
            $this->as = array('clasificacionesSearch'=>'clasificacion');
            $crud = $this->crud_function();
            if(!empty($_POST['idioma'])){
                $crud->where('clasificacion.idioma',$_POST['idioma']);
            }
            $crud->unset_delete()
                     ->unset_add()
                     ->unset_edit()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export();            
            $crud->render();  
            $this->loadView($crud);
        }
        
        public function funcionesSearch(){
            $this->as = array('funcionesSearch'=>'beverage_funciones');
            $crud = $this->crud_function();
            $crud->unset_delete()
                     ->unset_add()
                     ->unset_edit()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export();            
            $crud->render();  
            $this->loadView($crud);
        }
        
        function buscarFood(){
            $lista = new Bdsource();
            $lista->select = 'foods.*, gamas.icono_lista as gamafoto, gamas.gamas_nombre';
            $joins = array(
                array('foods_destinatarios','foods_destinatarios.foods_id = foods.id','left'),
                array('foods_aplicaciones','foods_aplicaciones.foods_id = foods.id','left'),
                array('gamas','gamas.id = foods.gamas_id','left')
            );                        
            $lista->filters = array('destinatarios_id','gamas_id','aplicaciones_id');
            if(!empty($_POST['descripcion'])){
                $lista->where('(foods.foods_nombre like "%'.$_POST['descripcion'].'%" OR MATCH(foods.foods_nombre) AGAINST ("'.$_POST['descripcion'].'") OR MATCH(foods.descripcion) AGAINST ("'.$_POST['descripcion'].'"))',NULL);
            }
            if(!empty($_POST['idioma'])){
                $lista->where('foods.idioma',$_POST['idioma']);
            }
            $lista->group_by('foods.id');
            $lista->joinNormal($joins);            
            $lista->init('foods');
            echo $lista->getJSON();
        }
        
        function buscarBeverage(){
            $lista = new Bdsource();
            $lista->select = 'beverages.*';            
            $lista->filters = array('tipos_id','clasificacion_id');
            foreach($this->db->get('beverage_funciones')->result() as $f){
                array_push($lista->filters,$f->url);
                if(!empty($_POST['funciones']) && $_POST['funciones'] == $f->url){
                    $_POST[$f->url] = 1;
                }
            }
            if(!empty($_POST['descripcion'])){
                $lista->where('(beverages.beverage_nombre like "%'.$_POST['descripcion'].'%" OR MATCH(beverages.beverage_nombre) AGAINST ("'.$_POST['descripcion'].'") OR MATCH(beverages.descripcion) AGAINST ("'.$_POST['descripcion'].'"))',NULL);
            }
            if(!empty($_POST['idioma'])){
                $lista->where('beverages.idioma',$_POST['idioma']);
            }
            $lista->group_by('beverages.id');            
            $lista->init('beverages');
            echo $lista->getJSON();
        }
        
        function contactar(){
            $this->form_validation->set_rules('nombre','Nombre','required');
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('mensaje','Mensaje','required');
            if($this->form_validation->run()){
                $str = '<h1>Hola, estan tratando de comunicarse desde ensiss</h1>';
                foreach($_POST as $n=>$p){
                    $str.= '<p><b>'.$n.': </b> '.$p.'</p>';
                }
                correo('r.sanz@ensissciences.com','Solicitud de contacto',$str);
            }
        }
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
