<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = "main";
$route['p/(:any)'] = "paginas/frontend/read/$1";

$route['e/(:any)'] = "productos/frontend/read/$1";
$route['e/productos'] = "productos/frontend/productos";
$route['n/(:any)'] = "noticias/frontend/read/$1";
$route['n/noticias'] = "noticias/frontend/noticias";
$route['b/(:any)'] = "blog/frontend/read/$1";
$route['b/blog'] = "blog/frontend/blog";
$route['pr/(:any)'] = "prensa/frontend/read/$1";
$route['pr/prensa'] = "prensa/frontend/prensa";
$route['c/contactenos'] = "contactenos/frontend/contactenos";
$route['s/soporte'] = "soporte/frontend/soporte";
$route['404_override'] = 'main/error404';
$route['403_override'] = 'main/error403';


/* End of file routes.php */
/* Location: ./application/config/routes.php */
