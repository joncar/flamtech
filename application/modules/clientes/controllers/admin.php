<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function clientes(){
            $crud = $this->crud_function('','');
            if($crud->getParameters()=='list'){
                $crud->set_field_upload('logo','img/clientes');
            }else{
                $crud->field_type('logo','image',array('path'=>'img/clientes','width'=>'250px','height'=>'150px'));
            }
            $crud = $crud->render();
            $this->loadView($crud);
        }        
    }