<?php $this->load->view('includes/headerMain'); ?>
<div style="background-image: url(<?= base_url('img/banner.jpg') ?>); width: 100%; height: 41px;"></div>
<div class="l-constrained l-content-wrap site-main">
    <div class="l-constrained mob-sub-nav">
        <div class="widget"><a class="js-scroll-to btn outline" href="#aside-menus">More <i class="fa fa-angle-down"></i></a></div>
    </div>
    <main role="main" class="l-main">
        <article class="article post-1301 page type-page status-publish hentry" id="post-1301">
            <div class="entry-content">
                <?= $this->db->get_where('paginas',array('titulo'=>'contactenos','idioma'=>$_SESSION['lang']))->row()->contenido ?>
                <div id="property_map" style="width:100%; height:300px;"></div>
            </div>
        </article>
    </main>
    <?php $this->load->view('_aside'); ?>
</div>
<script src="http://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places,geometry&sensor=true"></script>
<script>
    <?php 
        $map = $this->db->get_where('ajustes')->row()->mapa_contacto;
        $map = str_replace('(','',$map);
        $map = str_replace(')','',$map);
        $map = explode(',',$map);
        echo 'var lat = "'.$map[0].'", lon = "'.$map[1].'"; ';
    ?>
    var mapOptions = {
        zoom: 13,
        center: new google.maps.LatLng(lat,lon)
    };   
    map = new google.maps.Map(document.getElementById('property_map'), mapOptions);   
    new google.maps.Marker({ position: new google.maps.LatLng(lat,lon), map: map, title: 'FLAMTECH' });
</script>