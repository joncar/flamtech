<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();            
        }        
        
        function contactenos(){
            $this->loadView(array('view'=>'contactenos','title'=>'Contactenos'));
        }      
        
        function contacto(){            
            $this->form_validation->set_rules('nombre','Nombre y Apellido','required');
            $this->form_validation->set_rules('telefono','Telefono','required');
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('mensaje','Mensaje','required');
            $msj = '';
            if($this->form_validation->run()){
                $correo = '';
                foreach($_POST as $n=>$p){
                    $correo.= '<p><b>'.ucfirst($n).'</b> '.$p.'</p>';
                }
                correo('joncar.c@gmail.com','Solicitud de contacto',$correo);
                correo('info@flamtech.com.ve','Solicitud de contacto',$correo);
                $msj.= '<div class="l-aside" style="background:lightgreen; text-align:center">Su requerimiento ha sido recibido, en breve le contactaremos</div>';
            }else{
                $msj.= '<div class="l-aside" style="background:red; color:white; text-align:center">'.$this->form_validation->error_string().'</div>';
            }
            $this->loadView(array('view'=>'contactenos','title'=>'Contactenos','msj'=>$msj));
        }
    }
?>
