<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function paginas(){
            $crud = $this->crud_function('','');            
            $crud->columns('titulo','idioma');
            $crud->field_type('idioma','dropdown',array('es'=>'Español','en'=>'Ingles'));
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function banner(){
            $crud = $this->crud_function('','');            
            $crud->columns('foto','titulo','idioma');
            if($crud->getParameters()!='list'){
                $crud->field_type('foto','image',array('width'=>'600px','height'=>'300px','path'=>'img/banner'));
            }else{
                $crud->set_field_upload('foto','img/banner');
            }
            $crud->field_type('idioma','dropdown',array('es'=>'Español','en'=>'Ingles'));
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
