<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();            
        }        
        
        function soporte(){
            $this->loadView(array('view'=>'soporte','title'=>'Soporte'));
        }
        
        function contactenos(){
            $this->form_validation->set_rules('areas_servicio_id','Area de Servicio','required|integer');
            $this->form_validation->set_rules('nombre','Nombre y Apellido','required');
            $this->form_validation->set_rules('telefono','Telefono','required');
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('mensaje','Mensaje','required');
            $msj = '';
            if($this->form_validation->run()){
                $data = $_POST;
                $data['cerrado'] = 0;
                $this->db->insert('soporte',$data);
                $msj.= '<div class="l-aside" style="background:lightgreen; text-align:center">Su requerimiento ha sido recibido, en breve le contactaremos</div>';
            }else{
                $msj.= '<div class="l-aside" style="background:red; color:white; text-align:center">'.$this->form_validation->error_string().'</div>';
            }
            $this->loadView(array('view'=>'soporte','title'=>'Soporte','msj'=>$msj));
        }
    }
?>
