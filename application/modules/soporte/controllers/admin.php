<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function areas_servicio(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Area de Servicio');            
            $crud = $crud->render();
            $crud->title = 'Area de Servicio';
            $this->loadView($crud);
        }
        
        function soporte(){
            $crud = $this->crud_function('','');
            $crud->display_as('areas_servicio_id','Area de servicio');
            if($crud->getParameters()=='list'){
                $crud->field_type('cerrado','dropdown',array('0'=>'<span style="color:green">Abierto</span>','1'=>'<span style="color:red">Cerrado</span>'));
            }else{
                $crud->field_type('cerrado','true_false',array('0'=>'Abierto','1'=>'Cerrado'));
            }
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
