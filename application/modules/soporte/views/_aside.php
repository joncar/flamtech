<?php 
    if(!empty($msj)):     
        echo $msj;
    endif;
?>
<form action="<?= base_url('soporte/frontend/contactenos') ?>" method="post">
    <div style="width:50%; margin:0 auto; text-align: center">
        <div id="aside-menus">
            <div class="widget subnav">
                <h2 class="widget__heading">Soporte</h2>
                <ul class="widget-list">
                    <li class="page_item page-item-1317">
                        <div class="input-group">
                            Area de Servicio
                            <?= form_dropdown_from_query('areas_servicio_id','areas_servicio','id','area_nombre',0,'style="width:100%; margin:5px 0;"') ?>
                        </div><!-- /input-group -->
                    </li>
                    <li class="page_item page-item-1317">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Nombre y Apellido" name="nombre">
                        </div><!-- /input-group -->
                    </li>
                    <li class="page_item page-item-1317">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Teléfono" name="telefono">
                        </div><!-- /input-group -->
                    </li>
                    <li class="page_item page-item-1317">
                        <div class="input-group">
                            <input type="email" class="form-control" placeholder="Email" name="email">
                        </div><!-- /input-group -->
                    </li>
                    <li class="page_item page-item-1317">
                        <div class="input-group">
                            <textarea class="form-control" name="mensaje">Mensaje</textarea>
                        </div><!-- /input-group -->
                    </li>    
                    <li class="page_item page-item-1317">
                        <button class="btn" type="submit" style="width:100%;"><i class="fa fa-envelope-o"></i> Enviar datos</button>
                    </li> 
                </ul>
            </div>        
        </div>
    </div>
</form>