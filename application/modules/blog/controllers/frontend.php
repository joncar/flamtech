<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
            if(!empty($_GET)){
                foreach($_GET as $n=>$g){
                    $this->db->like($n,$g);
                }
            }else{
                $_GET['titulo'] = '';
            }
            $this->db->order_by('fecha','DESC');
            $this->db->where('idioma',$_SESSION['lang']);
            get_instance()->noticias = $this->db->get_where('blog');
        }        
        
        function blog(){
            $this->loadView(array('view'=>'blog','title'=>'Blog'));
        }
        
        function read($title){
            $id = explode('-',$title);
            if(is_numeric($id[0])){
                $id = $id[0];
                $producto = $this->db->get_where('blog',array('id'=>$id));
                if($producto->num_rows>0){
                    $producto = $producto->row();
                    $this->loadView(array('view'=>'read','title'=>$producto->titulo,'detail'=>$producto));
                    
                }else{
                    throw new Exception('La web que desea buscar no se encuentra disponible en este momento',404);
                }
            }else{
                throw new Exception('La web que desea buscar no se encuentra disponible en este momento',404);
            }            
        }
    }
?>
