<?php $this->load->view('includes/headerMain'); ?>
<div style="background-image: url(<?= base_url('img/banner.jpg') ?>); width: 100%; height: 41px;"></div>
<div class="l-constrained l-content-wrap site-main">
    <div class="l-constrained mob-sub-nav">
        <div class="widget"><a class="js-scroll-to btn outline" href="#aside-menus">More <i class="fa fa-angle-down"></i></a></div>
    </div>
    <main role="main" class="l-main">
        <article class="article post-1301 page type-page status-publish hentry" id="post-1301">
            <div class="entry-content">
                <div style="display:inline-block; width:50%; vertical-align: top">
                    <h1><?= $detail->titulo ?></h1>
                    <?= $detail->contenido ?>
                </div>
                <div style="display:inline-block; width:49%;">
                    <?= img('img/prensa/'.$detail->foto,'width:100%'); ?>
                </div>
            </div>
        </article>
    </main>
    <?php $this->load->view('_aside'); ?>
</div>