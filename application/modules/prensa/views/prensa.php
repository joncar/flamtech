<?php $this->load->view('includes/headerMain'); ?>
<div style="background-image: url(<?= base_url('img/banner.jpg') ?>); width: 100%; height: 41px;"></div>
<div class="l-constrained l-content-wrap site-main">
    <div class="l-constrained mob-sub-nav">
        <div class="widget"><a class="js-scroll-to btn outline" href="#aside-menus">More <i class="fa fa-angle-down"></i></a></div>
    </div>
    <main role="main" class="l-main">
        <article class="article post-1301 page type-page status-publish hentry" id="post-1301">
            <div class="entry-content">
                <?php if($this->noticias->num_rows==0): ?>
                    No se encontraron noticias para sus críterios de búsqueda.
                <?php endif ?>
                <?php foreach($this->noticias->result() as $p): ?>
                    <a href="<?= site_url('pr/'.toUrl($p->id.'-'.$p->titulo)) ?>" style="margin:10px; display: inline-block; width:25%;">
                        <img src="<?= base_url('img/prensa/'.$p->foto) ?>">
                        <div style="margin:5px 0; text-align: center;"><?= $p->titulo ?></div>
                    </a>
                <?php endforeach ?>
            </div>
        </article>
    </main>
    <?php $this->load->view('_aside'); ?>
</div>