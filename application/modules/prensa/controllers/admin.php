<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function prensa(){
            $crud = $this->crud_function('','');            
            $crud->columns('titulo','idioma');
            $crud->field_type('idioma','dropdown',array('es'=>'Español','en'=>'Ingles'));
            if($crud->getParameters()=='list' || $crud->getParameters()=='delete'){
                $crud->set_field_upload('foto','img/prensa');
            }else{
                $crud->field_type('foto','image',array('path'=>'img/prensa','width'=>'300px','height'=>'300px'));
            }
            $crud = $crud->render();
            $this->loadView($crud);
        }        
    }
?>
