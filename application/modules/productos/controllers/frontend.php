<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
            if(!empty($_GET)){
                foreach($_GET as $n=>$g){
                    $this->db->like($n,$g);
                }
            }else{
                $_GET['nombre'] = '';
            }
            $this->db->where('idioma',$_SESSION['lang']);
            get_instance()->productos = $this->db->get_where('productos');
        }        
        
        function productos(){
            $this->loadView(array('view'=>'productos','title'=>'Galería de Productos'));
        }
        
        function read($title){
            $id = explode('-',$title);
            if(is_numeric($id[0])){
                $id = $id[0];
                $producto = $this->db->get_where('productos',array('id'=>$id));
                if($producto->num_rows>0){
                    $producto = $producto->row();
                    $this->loadView(array('view'=>'read','title'=>$producto->nombre,'detail'=>$producto));
                    
                }else{
                    throw new Exception('La web que desea buscar no se encuentra disponible en este momento',404);
                }
            }else{
                throw new Exception('La web que desea buscar no se encuentra disponible en este momento',404);
            }            
        }
    }
?>
