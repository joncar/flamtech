<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function productos(){
            $crud = $this->crud_function('','');            
            $crud->columns('nombre','idioma','publicar_home');
            $crud->field_type('idioma','dropdown',array('es'=>'Español','en'=>'Ingles'));
            if($crud->getParameters()=='list'){
                $crud->set_field_upload('foto','img/productos');
            }else{
                $crud->field_type('foto','image',array('path'=>'img/productos','width'=>'300px','height'=>'300px'));
            }
            $crud = $crud->render();
            $this->loadView($crud);
        }                
    }
?>
