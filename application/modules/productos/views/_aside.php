<form action="<?= base_url('e/productos') ?>" method="get">
    <div class="l-aside">
        <div id="aside-menus">
            <div class="widget subnav">
                <h2 class="widget__heading">Filtros</h2>
                <ul class="widget-list">
                    <li class="page_item page-item-1317">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Buscar" name="nombre" value="<?= $_GET['nombre'] ?>">
                        </div><!-- /input-group -->
                    </li>    
                    <li class="page_item page-item-1317">
                        <button class="btn" type="submit" style="width:100%;"><i class="fa fa-search"></i> Aplicar filtro</button>
                    </li> 
                </ul>
            </div>        
        </div>
    </div>
</form>