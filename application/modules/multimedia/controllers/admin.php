<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function multimedia(){
            $crud = $this->crud_function('','');
            $crud->columns('titulo');
            $crud->field_type('idioma','dropdown',array('es'=>'Español','en'=>'Ingles'));
            $crud = $crud->render();
            $this->loadView($crud);
        }        
    }
?>
