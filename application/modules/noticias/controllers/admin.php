<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function noticias(){
            $crud = $this->crud_function('','');            
            $crud->columns('titulo','idioma','tipo');
            $crud->field_type('idioma','dropdown',array('es'=>'Español','en'=>'Ingles'));
            if($crud->getParameters()=='list' || $crud->getParameters()=='delete'){
                $crud->set_field_upload('foto','img/noticias');
            }else{
                $crud->field_type('foto','image',array('path'=>'img/noticias','width'=>'300px','height'=>'300px'));
            }
            $crud->field_type('tipo','dropdown',array('1'=>'Noticia','2'=>'Evento'));
            $crud = $crud->render();
            $this->loadView($crud);
        }        
    }
?>
